﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLIVEIRAJESSICA_CODING001
{
    public class Guest
    {
        string name;
        string phone;
        string email;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }
        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public override string ToString()
        {
            return name;
        }
    }
}
