﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OLIVEIRAJESSICA_CODING001
{
    public partial class PartyList : Form
    {
        // Custom event handlers to be used throughout the coding exercise
        public event EventHandler AddToInvitedList;

        // Instantiate the second form for further use
        public GuestList myGuestList = null;

        // Lists to hold the Guest objects
        public List<Guest> invitedGuests = new List<Guest>();

        public Guest newInvitedGuests
        {
            get
            {
                Guest guestObject = new Guest();
                guestObject.Name = txtbox_name.Text;
                guestObject.Phone = txtbox_phone.Text;
                guestObject.Email = txtbox_email.Text;

                return guestObject;
            }
        }

        public PartyList()
        {
            InitializeComponent();
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveList = new SaveFileDialog();

            // Filter the type of file that will be saved
            saveList.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveList.FilterIndex = 1;
            saveList.RestoreDirectory = true;

            if (saveList.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamWriter sw = new StreamWriter(saveList.FileName))
                    {
                        sw.WriteLine("Invited Guests:");
                        foreach (Guest person in invitedGuests)
                        {
                            sw.WriteLine(person.Name);
                            sw.WriteLine(person.Phone);
                            sw.WriteLine(person.Email);
                        }

                        sw.WriteLine("Confirmed Guests:");
                        GuestList newClass = (GuestList)sender;
                        foreach (Guest person in newClass.confirmedGuests)
                        {
                            sw.WriteLine(person.Name);
                            sw.WriteLine(person.Phone);
                            sw.WriteLine(person.Email);
                        }
                    }
                }
                catch (IOException a)
                {
                    // A proper message will be displayed according to the error
                    MessageBox.Show(a.Message);
                }
            }
            else
            {
                // Displays the message below in case the user attempts to save an empty file
                MessageBox.Show("Can't save empty fields!");
            }
        }

        private void Reset_Click(object sender, EventArgs e)
        {
            txtbox_name.Text = "";
            txtbox_phone.Text = "";
            txtbox_email.Text = "";
        }

        private void button_addGuest_Click(object sender, EventArgs e)
        {
            invitedGuests.Add(newInvitedGuests);
            if (AddToInvitedList != null)
            {
                AddToInvitedList(this, new EventArgs());
            }
        }

        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(Application.OpenForms.OfType<GuestList>().Count() == 0)
            {
                myGuestList = new GuestList();
                AddToInvitedList += myGuestList.AddNewGuestToInvitedList;
                if (AddToInvitedList != null)
                {
                    AddToInvitedList(this, new EventArgs());
                }

                myGuestList.Remove += MyGuestList_Remove;
                myGuestList.MoveToFirstList += MyGuestList_MoveToFirstList;
                RepopulateConfirmedList += myGuestList.RepopulateConfirmedList;

                myGuestList.Show();
            }
        }

        private void MyGuestList_Remove(object sender, GuestList.DeleteFromFirstListEventArgs e)
        {
            invitedGuests.RemoveAt(e.index);
        }

        private void MyGuestList_MoveToFirstList(object sender, EventArgs e)
        {
            GuestList whateverGuest = (GuestList)sender;
            invitedGuests.Add(whateverGuest.GetLastGuest);
        }
    }
}
