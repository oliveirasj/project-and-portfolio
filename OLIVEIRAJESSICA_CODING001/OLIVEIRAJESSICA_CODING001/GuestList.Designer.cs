﻿namespace OLIVEIRAJESSICA_CODING001
{
    partial class GuestList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox_partyList = new System.Windows.Forms.GroupBox();
            this.button_remove = new System.Windows.Forms.Button();
            this.button_addToFirstList = new System.Windows.Forms.Button();
            this.button_addToSecondList = new System.Windows.Forms.Button();
            this.label_confirmed = new System.Windows.Forms.Label();
            this.label_invited = new System.Windows.Forms.Label();
            this.lbox_confirmed = new System.Windows.Forms.ListBox();
            this.lbox_invited = new System.Windows.Forms.ListBox();
            this.groupBox_partyList.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox_partyList
            // 
            this.groupBox_partyList.Controls.Add(this.button_remove);
            this.groupBox_partyList.Controls.Add(this.button_addToFirstList);
            this.groupBox_partyList.Controls.Add(this.button_addToSecondList);
            this.groupBox_partyList.Controls.Add(this.label_confirmed);
            this.groupBox_partyList.Controls.Add(this.label_invited);
            this.groupBox_partyList.Controls.Add(this.lbox_confirmed);
            this.groupBox_partyList.Controls.Add(this.lbox_invited);
            this.groupBox_partyList.Location = new System.Drawing.Point(0, 0);
            this.groupBox_partyList.Name = "groupBox_partyList";
            this.groupBox_partyList.Size = new System.Drawing.Size(1150, 752);
            this.groupBox_partyList.TabIndex = 0;
            this.groupBox_partyList.TabStop = false;
            this.groupBox_partyList.Text = "Party List";
            // 
            // button_remove
            // 
            this.button_remove.Location = new System.Drawing.Point(529, 530);
            this.button_remove.Name = "button_remove";
            this.button_remove.Size = new System.Drawing.Size(89, 71);
            this.button_remove.TabIndex = 26;
            this.button_remove.Text = "X";
            this.button_remove.UseVisualStyleBackColor = true;
            this.button_remove.Click += new System.EventHandler(this.button_remove_Click);
            // 
            // button_addToFirstList
            // 
            this.button_addToFirstList.Location = new System.Drawing.Point(529, 355);
            this.button_addToFirstList.Name = "button_addToFirstList";
            this.button_addToFirstList.Size = new System.Drawing.Size(89, 71);
            this.button_addToFirstList.TabIndex = 25;
            this.button_addToFirstList.Text = "<<";
            this.button_addToFirstList.UseVisualStyleBackColor = true;
            this.button_addToFirstList.Click += new System.EventHandler(this.button_addToFirstList_Click);
            // 
            // button_addToSecondList
            // 
            this.button_addToSecondList.Location = new System.Drawing.Point(529, 189);
            this.button_addToSecondList.Name = "button_addToSecondList";
            this.button_addToSecondList.Size = new System.Drawing.Size(89, 71);
            this.button_addToSecondList.TabIndex = 24;
            this.button_addToSecondList.Text = ">>";
            this.button_addToSecondList.UseVisualStyleBackColor = true;
            this.button_addToSecondList.Click += new System.EventHandler(this.button_addToSecondList_Click);
            // 
            // label_confirmed
            // 
            this.label_confirmed.AutoSize = true;
            this.label_confirmed.Location = new System.Drawing.Point(652, 53);
            this.label_confirmed.Name = "label_confirmed";
            this.label_confirmed.Size = new System.Drawing.Size(184, 25);
            this.label_confirmed.TabIndex = 23;
            this.label_confirmed.Text = "Confirmed Guests";
            // 
            // label_invited
            // 
            this.label_invited.AutoSize = true;
            this.label_invited.Location = new System.Drawing.Point(21, 53);
            this.label_invited.Name = "label_invited";
            this.label_invited.Size = new System.Drawing.Size(149, 25);
            this.label_invited.TabIndex = 22;
            this.label_invited.Text = "Invited Guests";
            // 
            // lbox_confirmed
            // 
            this.lbox_confirmed.FormattingEnabled = true;
            this.lbox_confirmed.ItemHeight = 25;
            this.lbox_confirmed.Location = new System.Drawing.Point(657, 106);
            this.lbox_confirmed.Name = "lbox_confirmed";
            this.lbox_confirmed.Size = new System.Drawing.Size(472, 629);
            this.lbox_confirmed.TabIndex = 21;
            // 
            // lbox_invited
            // 
            this.lbox_invited.FormattingEnabled = true;
            this.lbox_invited.ItemHeight = 25;
            this.lbox_invited.Location = new System.Drawing.Point(26, 106);
            this.lbox_invited.Name = "lbox_invited";
            this.lbox_invited.Size = new System.Drawing.Size(472, 629);
            this.lbox_invited.TabIndex = 20;
            // 
            // GuestList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1160, 765);
            this.Controls.Add(this.groupBox_partyList);
            this.Name = "GuestList";
            this.Text = "List";
            this.groupBox_partyList.ResumeLayout(false);
            this.groupBox_partyList.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_partyList;
        private System.Windows.Forms.Button button_remove;
        private System.Windows.Forms.Button button_addToFirstList;
        private System.Windows.Forms.Button button_addToSecondList;
        private System.Windows.Forms.Label label_confirmed;
        private System.Windows.Forms.Label label_invited;
        private System.Windows.Forms.ListBox lbox_confirmed;
        private System.Windows.Forms.ListBox lbox_invited;
    }
}