﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OLIVEIRAJESSICA_CODING001
{
    public partial class GuestList : Form
    {
        // List to hold the confirmed guests
        public List<Guest> confirmedGuests = new List<Guest>();

        public GuestList()
        {
            InitializeComponent();
        }

        // Create an event handler to delete guest from lbox_invited
        public event EventHandler MoveToFirstList;
        public event EventHandler<DeleteFromFirstListEventArgs> Remove;

        public class DeleteFromFirstListEventArgs : EventArgs
        {
            public int index;

            public DeleteFromFirstListEventArgs (int newIndex)
            {
                this.index = newIndex;
            }
        }

        // Create getter to get the selected index in lbox_Invited
        public int GetListIndex
        {
            get
            {
                return lbox_invited.SelectedIndex;
            }
        }

        public Guest GetLastGuest
        {
            get
            {
                return (Guest)lbox_invited.Items[lbox_invited.Items.Count - 1];
            }
        }

        // Code to repopulate confirmed listbox with the list
        public void RepopulateConfirmedList(object sender, EventArgs e)
        {
            lbox_invited.Items.Clear();
            for (int i = 0; i < confirmedGuests.Count; i++)
            {
                lbox_confirmed.Items.Add(confirmedGuests[i]);
            }
        }

        private void button_addToSecondList_Click(object sender, EventArgs e)
        {
            if (lbox_invited.SelectedItem != null)
            {
                // Moves the item from the "invited guests" list to the "confirmed guest" list
                lbox_confirmed.Items.Add(lbox_invited.SelectedItem);
                lbox_invited.Items.Remove(lbox_invited.SelectedItem);

                confirmedGuests.Clear();
                if (lbox_confirmed.Items.Count != 0)
                {
                    for(int i = 0; i < lbox_confirmed.Items.Count; i++)
                    {
                        confirmedGuests.Add((Guest)lbox_confirmed.Items[i]);
                    }
                    
                }
            }
            else if (lbox_confirmed.SelectedItem != null)
            {
                MessageBox.Show("This guest is already in this list!");
            }
        }

        private void button_addToFirstList_Click(object sender, EventArgs e)
        {
            if (lbox_confirmed.SelectedItem != null)
            {
                // Moves the item from the "confirmed guest" list to the "invited guest" list
                lbox_invited.Items.Add(lbox_confirmed.SelectedItem);
                if (MoveToFirstList != null)
                {
                    MoveToFirstList(this, new EventArgs());
                }
                lbox_confirmed.Items.Remove(lbox_confirmed.SelectedItem);

                confirmedGuests.Clear();
                if (lbox_confirmed.Items.Count != 0)
                {
                    for (int i = 0; i < lbox_confirmed.Items.Count; i++)
                    {
                        confirmedGuests.Add((Guest)lbox_confirmed.Items[i]);
                    }

                }
            }
            else if (lbox_invited.SelectedItem != null)
            {
                MessageBox.Show("This guest is already in this list!");
            }
        }

        private void button_remove_Click(object sender, EventArgs e)
        {
            // Verify which list the user selected the guest from to remove
            if (lbox_confirmed.SelectedItem != null)
            {
                if (Remove != null)
                {
                    Remove(this, new EventArgs());
                }
                lbox_confirmed.Items.Remove(lbox_confirmed.SelectedItem);
            }
            else if (lbox_invited.SelectedItem != null)
            {
                lbox_invited.Items.Remove(lbox_invited.SelectedItem);

                confirmedGuests.Clear();
                if (lbox_confirmed.Items.Count != 0)
                {
                    for (int i = 0; i < lbox_confirmed.Items.Count; i++)
                    {
                        confirmedGuests.Add((Guest)lbox_confirmed.Items[i]);
                    }
                }
            }
        }

        public void AddNewGuestToInvitedList(object sender, EventArgs e)
        {
            try
            {
                lbox_invited.Items.Clear();
                PartyList newObject = (PartyList)sender;

                for (int i = 0; i < newObject.invitedGuests.Count; i++)
                {
                    lbox_invited.Items.Add(newObject.invitedGuests[i]);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
