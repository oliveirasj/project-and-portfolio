﻿namespace OLIVEIRAJESSICA_CODING001
{
    partial class PartyList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Save = new System.Windows.Forms.ToolStripMenuItem();
            this.Reset = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.listToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox_guestInformation = new System.Windows.Forms.GroupBox();
            this.button_addGuest = new System.Windows.Forms.Button();
            this.txtbox_email = new System.Windows.Forms.TextBox();
            this.txtbox_phone = new System.Windows.Forms.TextBox();
            this.txtbox_name = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label_phonenumber = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox_guestInformation.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.listToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(664, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Save,
            this.Reset,
            this.Exit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // Save
            // 
            this.Save.Name = "Save";
            this.Save.ShortcutKeyDisplayString = "CTRL+S";
            this.Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.Save.Size = new System.Drawing.Size(269, 38);
            this.Save.Text = "Save";
            // 
            // Reset
            // 
            this.Reset.Name = "Reset";
            this.Reset.ShortcutKeyDisplayString = "CTRL+R";
            this.Reset.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.R)));
            this.Reset.Size = new System.Drawing.Size(269, 38);
            this.Reset.Text = "Reset";
            this.Reset.Click += new System.EventHandler(this.Reset_Click);
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.ShortcutKeyDisplayString = "CTRL+Q";
            this.Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.Exit.Size = new System.Drawing.Size(269, 38);
            this.Exit.Text = "Exit";
            // 
            // listToolStripMenuItem
            // 
            this.listToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem});
            this.listToolStripMenuItem.Name = "listToolStripMenuItem";
            this.listToolStripMenuItem.Size = new System.Drawing.Size(62, 36);
            this.listToolStripMenuItem.Text = "List";
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(191, 38);
            this.displayToolStripMenuItem.Text = "Display";
            this.displayToolStripMenuItem.Click += new System.EventHandler(this.displayToolStripMenuItem_Click);
            // 
            // groupBox_guestInformation
            // 
            this.groupBox_guestInformation.Controls.Add(this.button_addGuest);
            this.groupBox_guestInformation.Controls.Add(this.txtbox_email);
            this.groupBox_guestInformation.Controls.Add(this.txtbox_phone);
            this.groupBox_guestInformation.Controls.Add(this.txtbox_name);
            this.groupBox_guestInformation.Controls.Add(this.label5);
            this.groupBox_guestInformation.Controls.Add(this.label_phonenumber);
            this.groupBox_guestInformation.Controls.Add(this.label_name);
            this.groupBox_guestInformation.Location = new System.Drawing.Point(12, 62);
            this.groupBox_guestInformation.Name = "groupBox_guestInformation";
            this.groupBox_guestInformation.Size = new System.Drawing.Size(642, 340);
            this.groupBox_guestInformation.TabIndex = 1;
            this.groupBox_guestInformation.TabStop = false;
            this.groupBox_guestInformation.Text = "Guest Information";
            // 
            // button_addGuest
            // 
            this.button_addGuest.Location = new System.Drawing.Point(209, 247);
            this.button_addGuest.Name = "button_addGuest";
            this.button_addGuest.Size = new System.Drawing.Size(247, 77);
            this.button_addGuest.TabIndex = 21;
            this.button_addGuest.Text = "Add Guest";
            this.button_addGuest.UseVisualStyleBackColor = true;
            this.button_addGuest.Click += new System.EventHandler(this.button_addGuest_Click);
            // 
            // txtbox_email
            // 
            this.txtbox_email.Location = new System.Drawing.Point(209, 175);
            this.txtbox_email.Name = "txtbox_email";
            this.txtbox_email.Size = new System.Drawing.Size(378, 31);
            this.txtbox_email.TabIndex = 19;
            // 
            // txtbox_phone
            // 
            this.txtbox_phone.Location = new System.Drawing.Point(209, 115);
            this.txtbox_phone.Name = "txtbox_phone";
            this.txtbox_phone.Size = new System.Drawing.Size(378, 31);
            this.txtbox_phone.TabIndex = 18;
            // 
            // txtbox_name
            // 
            this.txtbox_name.Location = new System.Drawing.Point(209, 57);
            this.txtbox_name.Name = "txtbox_name";
            this.txtbox_name.Size = new System.Drawing.Size(378, 31);
            this.txtbox_name.TabIndex = 17;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(25, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 25);
            this.label5.TabIndex = 14;
            this.label5.Text = "Email address:";
            // 
            // label_phonenumber
            // 
            this.label_phonenumber.AutoSize = true;
            this.label_phonenumber.Location = new System.Drawing.Point(25, 118);
            this.label_phonenumber.Name = "label_phonenumber";
            this.label_phonenumber.Size = new System.Drawing.Size(161, 25);
            this.label_phonenumber.TabIndex = 13;
            this.label_phonenumber.Text = "Phone Number:";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(25, 60);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(74, 25);
            this.label_name.TabIndex = 12;
            this.label_name.Text = "Name:";
            // 
            // PartyList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(664, 411);
            this.Controls.Add(this.groupBox_guestInformation);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PartyList";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox_guestInformation.ResumeLayout(false);
            this.groupBox_guestInformation.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Save;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.GroupBox groupBox_guestInformation;
        private System.Windows.Forms.Button button_addGuest;
        private System.Windows.Forms.TextBox txtbox_email;
        private System.Windows.Forms.TextBox txtbox_phone;
        private System.Windows.Forms.TextBox txtbox_name;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_phonenumber;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.ToolStripMenuItem Reset;
        private System.Windows.Forms.ToolStripMenuItem listToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
    }
}

