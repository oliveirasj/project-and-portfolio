﻿namespace OLIVEIRAJESSICA_ASSIGNMENT02
{
    partial class ContactList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ContactList));
            this.gBox_contactList = new System.Windows.Forms.GroupBox();
            this.btn_edit = new System.Windows.Forms.Button();
            this.btn_remove = new System.Windows.Forms.Button();
            this.listView_ContactList = new System.Windows.Forms.ListView();
            this.imageList_LargeIcons = new System.Windows.Forms.ImageList(this.components);
            this.imageList_SmallIcons = new System.Windows.Forms.ImageList(this.components);
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.displayToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.smallIconsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.largeIconsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gBox_contactList.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gBox_contactList
            // 
            this.gBox_contactList.Controls.Add(this.btn_edit);
            this.gBox_contactList.Controls.Add(this.btn_remove);
            this.gBox_contactList.Controls.Add(this.listView_ContactList);
            this.gBox_contactList.Location = new System.Drawing.Point(21, 58);
            this.gBox_contactList.Name = "gBox_contactList";
            this.gBox_contactList.Size = new System.Drawing.Size(576, 893);
            this.gBox_contactList.TabIndex = 0;
            this.gBox_contactList.TabStop = false;
            this.gBox_contactList.Text = "Contact List";
            // 
            // btn_edit
            // 
            this.btn_edit.Location = new System.Drawing.Point(295, 797);
            this.btn_edit.Name = "btn_edit";
            this.btn_edit.Size = new System.Drawing.Size(268, 81);
            this.btn_edit.TabIndex = 2;
            this.btn_edit.Text = "Edit";
            this.btn_edit.UseVisualStyleBackColor = true;
            this.btn_edit.Click += new System.EventHandler(this.btn_edit_Click);
            // 
            // btn_remove
            // 
            this.btn_remove.Location = new System.Drawing.Point(14, 797);
            this.btn_remove.Name = "btn_remove";
            this.btn_remove.Size = new System.Drawing.Size(268, 81);
            this.btn_remove.TabIndex = 1;
            this.btn_remove.Text = "Remove";
            this.btn_remove.UseVisualStyleBackColor = true;
            this.btn_remove.Click += new System.EventHandler(this.btn_remove_Click);
            // 
            // listView_ContactList
            // 
            this.listView_ContactList.LargeImageList = this.imageList_LargeIcons;
            this.listView_ContactList.Location = new System.Drawing.Point(14, 49);
            this.listView_ContactList.Name = "listView_ContactList";
            this.listView_ContactList.Size = new System.Drawing.Size(549, 733);
            this.listView_ContactList.SmallImageList = this.imageList_SmallIcons;
            this.listView_ContactList.TabIndex = 0;
            this.listView_ContactList.UseCompatibleStateImageBehavior = false;
            // 
            // imageList_LargeIcons
            // 
            this.imageList_LargeIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_LargeIcons.ImageStream")));
            this.imageList_LargeIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_LargeIcons.Images.SetKeyName(0, "01 - A.png");
            this.imageList_LargeIcons.Images.SetKeyName(1, "02 - B.png");
            this.imageList_LargeIcons.Images.SetKeyName(2, "03 - C.png");
            this.imageList_LargeIcons.Images.SetKeyName(3, "04 - D.png");
            this.imageList_LargeIcons.Images.SetKeyName(4, "05 - E.png");
            this.imageList_LargeIcons.Images.SetKeyName(5, "06 - F.png");
            this.imageList_LargeIcons.Images.SetKeyName(6, "07 - G.png");
            this.imageList_LargeIcons.Images.SetKeyName(7, "08 - H.png");
            this.imageList_LargeIcons.Images.SetKeyName(8, "09 - I.png");
            this.imageList_LargeIcons.Images.SetKeyName(9, "10 - J.png");
            this.imageList_LargeIcons.Images.SetKeyName(10, "11 - K.png");
            this.imageList_LargeIcons.Images.SetKeyName(11, "12 - L.png");
            this.imageList_LargeIcons.Images.SetKeyName(12, "13 - M.png");
            this.imageList_LargeIcons.Images.SetKeyName(13, "14 - N.png");
            this.imageList_LargeIcons.Images.SetKeyName(14, "15 - O.png");
            this.imageList_LargeIcons.Images.SetKeyName(15, "16 - P.png");
            this.imageList_LargeIcons.Images.SetKeyName(16, "17 - Q.png");
            this.imageList_LargeIcons.Images.SetKeyName(17, "18 - R.png");
            this.imageList_LargeIcons.Images.SetKeyName(18, "19 - S.png");
            this.imageList_LargeIcons.Images.SetKeyName(19, "20 - T.png");
            this.imageList_LargeIcons.Images.SetKeyName(20, "21 - U.png");
            this.imageList_LargeIcons.Images.SetKeyName(21, "22 - V.png");
            this.imageList_LargeIcons.Images.SetKeyName(22, "23 - W.png");
            this.imageList_LargeIcons.Images.SetKeyName(23, "24 - X.png");
            this.imageList_LargeIcons.Images.SetKeyName(24, "25 - Y.png");
            this.imageList_LargeIcons.Images.SetKeyName(25, "26 - Z.png");
            // 
            // imageList_SmallIcons
            // 
            this.imageList_SmallIcons.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList_SmallIcons.ImageStream")));
            this.imageList_SmallIcons.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList_SmallIcons.Images.SetKeyName(0, "01 - A.png");
            this.imageList_SmallIcons.Images.SetKeyName(1, "02 - B.png");
            this.imageList_SmallIcons.Images.SetKeyName(2, "03 - C.png");
            this.imageList_SmallIcons.Images.SetKeyName(3, "04 - D.png");
            this.imageList_SmallIcons.Images.SetKeyName(4, "05 - E.png");
            this.imageList_SmallIcons.Images.SetKeyName(5, "06 - F.png");
            this.imageList_SmallIcons.Images.SetKeyName(6, "07 - G.png");
            this.imageList_SmallIcons.Images.SetKeyName(7, "08 - H.png");
            this.imageList_SmallIcons.Images.SetKeyName(8, "09 - I.png");
            this.imageList_SmallIcons.Images.SetKeyName(9, "10 - J.png");
            this.imageList_SmallIcons.Images.SetKeyName(10, "11 - K.png");
            this.imageList_SmallIcons.Images.SetKeyName(11, "12 - L.png");
            this.imageList_SmallIcons.Images.SetKeyName(12, "13 - M.png");
            this.imageList_SmallIcons.Images.SetKeyName(13, "14 - N.png");
            this.imageList_SmallIcons.Images.SetKeyName(14, "15 - O.png");
            this.imageList_SmallIcons.Images.SetKeyName(15, "16 - P.png");
            this.imageList_SmallIcons.Images.SetKeyName(16, "17 - Q.png");
            this.imageList_SmallIcons.Images.SetKeyName(17, "18 - R.png");
            this.imageList_SmallIcons.Images.SetKeyName(18, "19 - S.png");
            this.imageList_SmallIcons.Images.SetKeyName(19, "20 - T.png");
            this.imageList_SmallIcons.Images.SetKeyName(20, "21 - U.png");
            this.imageList_SmallIcons.Images.SetKeyName(21, "22 - V.png");
            this.imageList_SmallIcons.Images.SetKeyName(22, "23 - W.png");
            this.imageList_SmallIcons.Images.SetKeyName(23, "24 - X.png");
            this.imageList_SmallIcons.Images.SetKeyName(24, "25 - Y.png");
            this.imageList_SmallIcons.Images.SetKeyName(25, "26 - Z.png");
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.displayToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(610, 40);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // displayToolStripMenuItem
            // 
            this.displayToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.smallIconsToolStripMenuItem,
            this.largeIconsToolStripMenuItem});
            this.displayToolStripMenuItem.Name = "displayToolStripMenuItem";
            this.displayToolStripMenuItem.Size = new System.Drawing.Size(104, 36);
            this.displayToolStripMenuItem.Text = "Display";
            // 
            // smallIconsToolStripMenuItem
            // 
            this.smallIconsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.smallIconsToolStripMenuItem.Name = "smallIconsToolStripMenuItem";
            this.smallIconsToolStripMenuItem.Size = new System.Drawing.Size(234, 38);
            this.smallIconsToolStripMenuItem.Text = "Small Icons";
            this.smallIconsToolStripMenuItem.Click += new System.EventHandler(this.smallIconsToolStripMenuItem_Click);
            // 
            // largeIconsToolStripMenuItem
            // 
            this.largeIconsToolStripMenuItem.Checked = true;
            this.largeIconsToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.largeIconsToolStripMenuItem.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.largeIconsToolStripMenuItem.Name = "largeIconsToolStripMenuItem";
            this.largeIconsToolStripMenuItem.Size = new System.Drawing.Size(234, 38);
            this.largeIconsToolStripMenuItem.Text = "Large Icons";
            this.largeIconsToolStripMenuItem.Click += new System.EventHandler(this.largeIconsToolStripMenuItem_Click);
            // 
            // ContactList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(610, 961);
            this.Controls.Add(this.gBox_contactList);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "ContactList";
            this.Text = "ContactList";
            this.gBox_contactList.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gBox_contactList;
        private System.Windows.Forms.ListView listView_ContactList;
        private System.Windows.Forms.ImageList imageList_LargeIcons;
        private System.Windows.Forms.ImageList imageList_SmallIcons;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem displayToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem smallIconsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem largeIconsToolStripMenuItem;
        private System.Windows.Forms.Button btn_edit;
        private System.Windows.Forms.Button btn_remove;
    }
}