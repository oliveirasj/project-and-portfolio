﻿// NAME: JESSICA OLIVEIRA
// CLASS: PROJECT AND PORTFOLIO III
// TERM: JANUARY 2018
// EXERCISE: CONTACT LIST (ASSIGNMENT 2)
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.IO;

namespace OLIVEIRAJESSICA_ASSIGNMENT02
{
    public partial class MainForm : Form
    {
        // A list to hold all "Contact" objects created
        public List<CreateContact> contactList = new List<CreateContact>();

        // An event handler to use with the "Add Contact"
        public event EventHandler<CreateContact> AddNewContact;
        public event EventHandler<CreateContact> SaveEditedContactToListView;
        public event EventHandler ClearListViewWhenReopenSecondForm;

        // A char array that holds all letters from the alphabet. This will be used to compare the first letter from the contact's last name and find the correct icon to assign to it.
        string[] alphabet = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z" };

        // Variables and objects to be used throughout the code
        int index;
        CreateContact contact;
        bool Is1stNameCorrect = false;
        bool Is2ndNameCorrect = false;
        bool IsEmailCorrect = false;
        bool IsPhoneCorrect = false;
        bool editBool = false;
        ContactList contactListObj = null;

        public class CreateContact : EventArgs
        {
            // Variables to hold the values of the user input fields
            public string _firstname;
            public string _lastName;
            public string _phone;
            public string _email;
            public int _iconIndex;

            // Constructor assigns the values to the instance
            public CreateContact(string firstname, string lastname, string phone, string email, int iconindex)
            {
                _firstname = firstname;
                _lastName = lastname;
                _phone = phone;
                _email = email;
                _iconIndex = iconindex;
            }

            public override string ToString()
            {
                return $"{_firstname} {_lastName}";
            }
        }

        public MainForm()
        {
            InitializeComponent();

            // Make the error labels invisible when the program starts
            label_error_firstname.Visible = false;
            label_error_lastname.Visible = false;
            label_error_phone.Visible = false;
            label_error_email.Visible = false;
        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private int InitialLetter(string firstName)
        {
            // Gets the initial letter from the contact last name in order to select the appropriate icon for it
            char[] firstNameChar = firstName.ToCharArray();

            string initialLetter = firstNameChar[0].ToString().ToLower();

            // Verify which index of the array "alphabet" has the same value of the initial letter of the contact's last name
            for (int i = 0; i < alphabet.Length; i++)
            {
                if (alphabet[i] == initialLetter)
                {
                    index = i;
                }
            }

            return index;
        }

        private void btn_add_Click(object sender, EventArgs e)
        {
            if (txtBox_firstName.Text != "")
            {
                if (Application.OpenForms.OfType<ContactList>().Count() == 0)
                {
                    // Validate all the user entry fields
                    firstNameValidation(txtBox_firstName.Text);
                    lastNameValidation(txtBox_lastName.Text);
                    phoneValidation(mTxtBox_phone.Text);
                    emailValidation(txtBox_email.Text);

                    if (Is1stNameCorrect == true && Is2ndNameCorrect == true && IsPhoneCorrect == true && IsEmailCorrect == true)
                    {
                        // Creates a second form window
                        contactListObj = new ContactList();

                        // Invoke the other form's method where it's going to add the contact's information to the listview
                        AddNewContact += contactListObj.AddContactToListeView;
                        SaveEditedContactToListView += contactListObj.AddContactToListeViewEdited;
                        contactListObj.RemoveOnMainFormList += RemoveFromList;
                        contactListObj.RepopulateMainForm += RepopulateThisForm;
                        contactListObj.PassingBoolToEdit += ContactListObj_PassingBoolToEdit;
                        ClearListViewWhenReopenSecondForm += contactListObj.ClearTheListView;

                        if(editBool == false)
                        {
                            contact = new CreateContact(txtBox_firstName.Text, txtBox_lastName.Text, mTxtBox_phone.Text, txtBox_email.Text, index);

                            contactList.Add(contact);

                            ClearListViewWhenReopenSecondForm?.Invoke(this, new EventArgs());

                            for (int i = 0; i < contactList.Count; i++)
                            {
                                AddNewContact(this, new CreateContact(contactList[i]._firstname, contactList[i]._lastName, contactList[i]._phone, contactList[i]._email, InitialLetter(contactList[i]._firstname)));
                            }
                        }

                        btn_clear_Click(this, new EventArgs());

                        contactListObj.Show();

                        // Make the error labels invisible when the inputs are correct and make the textbox's letters go back to back
                        label_error_firstname.Visible = false;
                        label_error_lastname.Visible = false;
                        label_error_phone.Visible = false;
                        label_error_email.Visible = false;
                        txtBox_firstName.ForeColor = Color.Black;
                        txtBox_lastName.ForeColor = Color.Black;
                        mTxtBox_phone.ForeColor = Color.Black;
                        txtBox_email.ForeColor = Color.Black;
                    }
                }
                else if (Application.OpenForms.OfType<ContactList>().Count() == 1)
                {
                    if(editBool == false)
                    {
                        contact = new CreateContact(txtBox_firstName.Text, txtBox_lastName.Text, mTxtBox_phone.Text, txtBox_email.Text, index);

                        contactList.Add(contact);

                        ClearListViewWhenReopenSecondForm?.Invoke(this, new EventArgs());

                        for (int i = 0; i < contactList.Count; i++)
                        {
                            AddNewContact(this, new CreateContact(contactList[i]._firstname, contactList[i]._lastName, contactList[i]._phone, contactList[i]._email, InitialLetter(contactList[i]._firstname)));
                        }
                        btn_clear_Click(this, new EventArgs());
                    }
                    else if (editBool == true)
                    {
                        contactList.RemoveAt(contactListObj.SelectedIndexFromListView);

                        contact = new CreateContact(txtBox_firstName.Text, txtBox_lastName.Text, mTxtBox_phone.Text, txtBox_email.Text, index);

                        contactList.Insert(contactListObj.SelectedIndexFromListView, contact);

                        SaveEditedContactToListView?.Invoke(this, new CreateContact(txtBox_firstName.Text, txtBox_lastName.Text, mTxtBox_phone.Text, txtBox_email.Text, InitialLetter(txtBox_firstName.Text)));

                        editBool = false;
                    }
                }
            }
            else
            {
                MessageBox.Show("Please fill in the contact's information before adding!");
            }
        }

        private void ContactListObj_PassingBoolToEdit(object sender, ContactList.EditBoolean e)
        {
            editBool = e.editBool;
        }

        private void RepopulateThisForm(object sender, ContactList.ExistingContactInfo e)
        {
            txtBox_firstName.Text = contactList[e.contactInfoIndex]._firstname;
            txtBox_lastName.Text = contactList[e.contactInfoIndex]._lastName;
            mTxtBox_phone.Text = contactList[e.contactInfoIndex]._phone;
            txtBox_email.Text = contactList[e.contactInfoIndex]._email;
        }

        private void RemoveFromList(object sender, ContactList.SelectedItemIndex e)
        {
            contactList.RemoveAt(e.GetIndex);
        }

        private void btn_clear_Click(object sender, EventArgs e)
        {
            txtBox_firstName.Text = "";
            txtBox_lastName.Text = "";
            mTxtBox_phone.Text = "";
            txtBox_email.Text = "";
        }

        public string firstNameValidation(string name)
        {
            string pattern = @"^[a-zA-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)*$";

            if (!Regex.IsMatch(name, pattern))
            {
                txtBox_firstName.ForeColor = Color.Red;
                label_error_firstname.Visible = true;
            }
            else
            {
                Is1stNameCorrect = true;
            }

            return name;
        }

        public string lastNameValidation(string name)
        {
            string pattern = @"^[a-zA-Z]+(([\'\,\.\-][a-zA-Z])?[a-zA-Z]*)*$";

            if (!Regex.IsMatch(name, pattern))
            {
                txtBox_lastName.ForeColor = Color.Red;
                label_error_lastname.Visible = true;
            }
            else
            {
                Is2ndNameCorrect = true;
            }

            return name;
        }

        public string phoneValidation(string phone)
        {
            string pattern = @"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}";

            if (!Regex.IsMatch(phone, pattern))
            {
                mTxtBox_phone.ForeColor = Color.Red;
                label_error_phone.Visible = true;
            }
            else
            {
                IsPhoneCorrect = true;
            }

            return phone;
        }

        public bool emailValidation(string email)
        {
            try
            {
                MailAddress emailAddress = new MailAddress(email);
                IsEmailCorrect = true;

                return IsEmailCorrect;
            }
            catch (FormatException)
            {
                txtBox_email.ForeColor = Color.Red;
                label_error_email.Visible = true;

                return false;
            }
        }

        private void Save_Click(object sender, EventArgs e)
        {
            SaveFileDialog savetxt = new SaveFileDialog();

            // To filter the type of file that will be saved
            savetxt.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            savetxt.FilterIndex = 1;
            savetxt.RestoreDirectory = true;
            
            if(savetxt.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using(StreamWriter sr = new StreamWriter(savetxt.FileName))
                    {
                        for(int i = 0; i < contactList.Count; i++)
                        {
                            sr.WriteLine("##############################");
                            sr.WriteLine($"First name: {contactList[i]._firstname}");
                            sr.WriteLine($"Last name: {contactList[i]._lastName}");
                            sr.WriteLine($"Phone: {contactList[i]._phone}");
                            sr.WriteLine($"Email: {contactList[i]._email}");
                        }
                    }
                }
                catch (IOException a)
                {

                    MessageBox.Show(a.Message);
                }
            }
        }
    }
}
