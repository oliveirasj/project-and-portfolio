﻿namespace OLIVEIRAJESSICA_ASSIGNMENT02
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gBox_contactInfo = new System.Windows.Forms.GroupBox();
            this.btn_add = new System.Windows.Forms.Button();
            this.btn_clear = new System.Windows.Forms.Button();
            this.txtBox_email = new System.Windows.Forms.TextBox();
            this.txtBox_lastName = new System.Windows.Forms.TextBox();
            this.txtBox_firstName = new System.Windows.Forms.TextBox();
            this.mTxtBox_phone = new System.Windows.Forms.MaskedTextBox();
            this.label_email = new System.Windows.Forms.Label();
            this.label_phoneNumber = new System.Windows.Forms.Label();
            this.label_lastName = new System.Windows.Forms.Label();
            this.label_firstName = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.label_error_firstname = new System.Windows.Forms.Label();
            this.label_error_lastname = new System.Windows.Forms.Label();
            this.label_error_phone = new System.Windows.Forms.Label();
            this.label_error_email = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.ToolStripMenuItem();
            this.gBox_contactInfo.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gBox_contactInfo
            // 
            this.gBox_contactInfo.Controls.Add(this.label_error_email);
            this.gBox_contactInfo.Controls.Add(this.label_error_phone);
            this.gBox_contactInfo.Controls.Add(this.label_error_lastname);
            this.gBox_contactInfo.Controls.Add(this.label_error_firstname);
            this.gBox_contactInfo.Controls.Add(this.btn_add);
            this.gBox_contactInfo.Controls.Add(this.btn_clear);
            this.gBox_contactInfo.Controls.Add(this.txtBox_email);
            this.gBox_contactInfo.Controls.Add(this.txtBox_lastName);
            this.gBox_contactInfo.Controls.Add(this.txtBox_firstName);
            this.gBox_contactInfo.Controls.Add(this.mTxtBox_phone);
            this.gBox_contactInfo.Controls.Add(this.label_email);
            this.gBox_contactInfo.Controls.Add(this.label_phoneNumber);
            this.gBox_contactInfo.Controls.Add(this.label_lastName);
            this.gBox_contactInfo.Controls.Add(this.label_firstName);
            this.gBox_contactInfo.Location = new System.Drawing.Point(12, 68);
            this.gBox_contactInfo.Name = "gBox_contactInfo";
            this.gBox_contactInfo.Size = new System.Drawing.Size(604, 532);
            this.gBox_contactInfo.TabIndex = 0;
            this.gBox_contactInfo.TabStop = false;
            this.gBox_contactInfo.Text = "Contact Information";
            // 
            // btn_add
            // 
            this.btn_add.Location = new System.Drawing.Point(328, 450);
            this.btn_add.Name = "btn_add";
            this.btn_add.Size = new System.Drawing.Size(262, 67);
            this.btn_add.TabIndex = 4;
            this.btn_add.Text = "Add Contact";
            this.btn_add.UseVisualStyleBackColor = true;
            this.btn_add.Click += new System.EventHandler(this.btn_add_Click);
            // 
            // btn_clear
            // 
            this.btn_clear.Location = new System.Drawing.Point(45, 450);
            this.btn_clear.Name = "btn_clear";
            this.btn_clear.Size = new System.Drawing.Size(262, 67);
            this.btn_clear.TabIndex = 5;
            this.btn_clear.Text = "Clear";
            this.btn_clear.UseVisualStyleBackColor = true;
            this.btn_clear.Click += new System.EventHandler(this.btn_clear_Click);
            // 
            // txtBox_email
            // 
            this.txtBox_email.Location = new System.Drawing.Point(236, 337);
            this.txtBox_email.Name = "txtBox_email";
            this.txtBox_email.Size = new System.Drawing.Size(354, 31);
            this.txtBox_email.TabIndex = 3;
            // 
            // txtBox_lastName
            // 
            this.txtBox_lastName.Location = new System.Drawing.Point(236, 156);
            this.txtBox_lastName.Name = "txtBox_lastName";
            this.txtBox_lastName.Size = new System.Drawing.Size(354, 31);
            this.txtBox_lastName.TabIndex = 1;
            // 
            // txtBox_firstName
            // 
            this.txtBox_firstName.Location = new System.Drawing.Point(235, 56);
            this.txtBox_firstName.Name = "txtBox_firstName";
            this.txtBox_firstName.Size = new System.Drawing.Size(354, 31);
            this.txtBox_firstName.TabIndex = 0;
            // 
            // mTxtBox_phone
            // 
            this.mTxtBox_phone.Location = new System.Drawing.Point(236, 248);
            this.mTxtBox_phone.Mask = "(999) 000-0000";
            this.mTxtBox_phone.Name = "mTxtBox_phone";
            this.mTxtBox_phone.Size = new System.Drawing.Size(354, 31);
            this.mTxtBox_phone.TabIndex = 2;
            // 
            // label_email
            // 
            this.label_email.AutoSize = true;
            this.label_email.Location = new System.Drawing.Point(40, 340);
            this.label_email.Name = "label_email";
            this.label_email.Size = new System.Drawing.Size(156, 25);
            this.label_email.TabIndex = 3;
            this.label_email.Text = "Email Address:";
            // 
            // label_phoneNumber
            // 
            this.label_phoneNumber.AutoSize = true;
            this.label_phoneNumber.Location = new System.Drawing.Point(40, 251);
            this.label_phoneNumber.Name = "label_phoneNumber";
            this.label_phoneNumber.Size = new System.Drawing.Size(161, 25);
            this.label_phoneNumber.TabIndex = 2;
            this.label_phoneNumber.Text = "Phone Number:";
            // 
            // label_lastName
            // 
            this.label_lastName.AutoSize = true;
            this.label_lastName.Location = new System.Drawing.Point(40, 159);
            this.label_lastName.Name = "label_lastName";
            this.label_lastName.Size = new System.Drawing.Size(121, 25);
            this.label_lastName.TabIndex = 1;
            this.label_lastName.Text = "Last Name:";
            // 
            // label_firstName
            // 
            this.label_firstName.AutoSize = true;
            this.label_firstName.Location = new System.Drawing.Point(39, 59);
            this.label_firstName.Name = "label_firstName";
            this.label_firstName.Size = new System.Drawing.Size(122, 25);
            this.label_firstName.TabIndex = 0;
            this.label_firstName.Text = "First Name:";
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(626, 42);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Save,
            this.Exit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 38);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.ShortcutKeyDisplayString = "CTRL+Q";
            this.Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.Exit.Size = new System.Drawing.Size(268, 38);
            this.Exit.Text = "Exit";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // label_error_firstname
            // 
            this.label_error_firstname.AutoSize = true;
            this.label_error_firstname.ForeColor = System.Drawing.Color.Red;
            this.label_error_firstname.Location = new System.Drawing.Point(236, 110);
            this.label_error_firstname.Name = "label_error_firstname";
            this.label_error_firstname.Size = new System.Drawing.Size(349, 25);
            this.label_error_firstname.TabIndex = 6;
            this.label_error_firstname.Text = "* Numbers/symbols are not allowed";
            // 
            // label_error_lastname
            // 
            this.label_error_lastname.AutoSize = true;
            this.label_error_lastname.ForeColor = System.Drawing.Color.Red;
            this.label_error_lastname.Location = new System.Drawing.Point(236, 203);
            this.label_error_lastname.Name = "label_error_lastname";
            this.label_error_lastname.Size = new System.Drawing.Size(349, 25);
            this.label_error_lastname.TabIndex = 7;
            this.label_error_lastname.Text = "* Numbers/symbols are not allowed";
            // 
            // label_error_phone
            // 
            this.label_error_phone.AutoSize = true;
            this.label_error_phone.ForeColor = System.Drawing.Color.Red;
            this.label_error_phone.Location = new System.Drawing.Point(236, 296);
            this.label_error_phone.Name = "label_error_phone";
            this.label_error_phone.Size = new System.Drawing.Size(238, 25);
            this.label_error_phone.TabIndex = 8;
            this.label_error_phone.Text = "* Only numbers allowed";
            // 
            // label_error_email
            // 
            this.label_error_email.AutoSize = true;
            this.label_error_email.ForeColor = System.Drawing.Color.Red;
            this.label_error_email.Location = new System.Drawing.Point(236, 385);
            this.label_error_email.Name = "label_error_email";
            this.label_error_email.Size = new System.Drawing.Size(154, 25);
            this.label_error_email.TabIndex = 9;
            this.label_error_email.Text = "* Invalid format";
            // 
            // Save
            // 
            this.Save.Name = "Save";
            this.Save.ShortcutKeyDisplayString = "CTRL+S";
            this.Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.Save.Size = new System.Drawing.Size(268, 38);
            this.Save.Text = "Save";
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(626, 607);
            this.Controls.Add(this.gBox_contactInfo);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.gBox_contactInfo.ResumeLayout(false);
            this.gBox_contactInfo.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox gBox_contactInfo;
        private System.Windows.Forms.MaskedTextBox mTxtBox_phone;
        private System.Windows.Forms.Label label_email;
        private System.Windows.Forms.Label label_phoneNumber;
        private System.Windows.Forms.Label label_lastName;
        private System.Windows.Forms.Label label_firstName;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.Button btn_add;
        private System.Windows.Forms.Button btn_clear;
        private System.Windows.Forms.TextBox txtBox_email;
        private System.Windows.Forms.TextBox txtBox_lastName;
        private System.Windows.Forms.TextBox txtBox_firstName;
        private System.Windows.Forms.Label label_error_email;
        private System.Windows.Forms.Label label_error_phone;
        private System.Windows.Forms.Label label_error_lastname;
        private System.Windows.Forms.Label label_error_firstname;
        private System.Windows.Forms.ToolStripMenuItem Save;
    }
}

