﻿// NAME: JESSICA OLIVEIRA
// CLASS: PROJECT AND PORTFOLIO III
// TERM: JANUARY 2018
// EXERCISE: CONTACT LIST (ASSIGNMENT 2)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLIVEIRAJESSICA_ASSIGNMENT02
{
    public class Contact
    {
        string firstName;
        string lastName;
        string phone;
        string email;

        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }

        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }

        public string Phone
        {
            get { return phone; }
            set { phone = value; }
        }

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        public override string ToString()
        {
            return $"{firstName} {lastName}";
        }
    }
}
