﻿// NAME: JESSICA OLIVEIRA
// CLASS: PROJECT AND PORTFOLIO III
// TERM: JANUARY 2018
// EXERCISE: CONTACT LIST (ASSIGNMENT 2)
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace OLIVEIRAJESSICA_ASSIGNMENT02
{
    public partial class ContactList : Form
    {
        // Variables to be used throughout the code
        ListViewItem contact;

        // Event handlers
        public event EventHandler<SelectedItemIndex> RemoveOnMainFormList;
        public event EventHandler<ExistingContactInfo> RepopulateMainForm;
        public event EventHandler<EditBoolean> PassingBoolToEdit;

        // Variables to be used throughout the code
        bool editboolean = false;

        public class EditBoolean : EventArgs
        {
            public bool editBool;

            public EditBoolean(bool edit)
            {
                editBool = edit;
            }
        }
        
        public class SelectedItemIndex : EventArgs
        {
            public int index;

            public SelectedItemIndex(int selectedIndex)
            {
                index = selectedIndex;
            }

            public int GetIndex
            {
                get { return index; }
            }
        }

        public class ExistingContactInfo : EventArgs
        {
            public int contactInfoIndex;

            public ExistingContactInfo(int _contactInfoIndex)
            {
                contactInfoIndex = _contactInfoIndex;
            }
        }
        public ContactList()
        {
            InitializeComponent();
        }

        public void AddContactToListeView(object sender, MainForm.CreateContact e)
        {
            contact = new ListViewItem();

            // Add the contact passed to this class to the list view
            contact.ImageIndex = e._iconIndex;
            contact.Text = e.ToString();
            contact.Tag = e;

            listView_ContactList.Items.Add(contact);
        }

        public void ClearTheListView(object sender, EventArgs e)
        {
            listView_ContactList.Clear();
        }

        public void AddContactToListeViewEdited(object sender, MainForm.CreateContact e)
        {
            // Add the contact passed to this class to the list view
            listView_ContactList.SelectedItems[0].ImageIndex = e._iconIndex;
            listView_ContactList.SelectedItems[0].Text = e.ToString();
            listView_ContactList.SelectedItems[0].Tag = e;
        }

        private void smallIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Switches to small icons
            listView_ContactList.View = View.SmallIcon;
            smallIconsToolStripMenuItem.Checked = true;
            largeIconsToolStripMenuItem.Checked = false;
        }

        private void largeIconsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Switches to large icons
            listView_ContactList.View = View.LargeIcon;
            largeIconsToolStripMenuItem.Checked = true;
            smallIconsToolStripMenuItem.Checked = false;
        }

        private void btn_remove_Click(object sender, EventArgs e)
        {
            if(listView_ContactList.SelectedItems != null)
            {
                RemoveOnMainFormList?.Invoke(this, new SelectedItemIndex(listView_ContactList.SelectedIndices[0]));
                listView_ContactList.Items.RemoveAt(listView_ContactList.SelectedIndices[0]);
            }
        }

        private void btn_edit_Click(object sender, EventArgs e)
        {
            if (listView_ContactList.SelectedItems != null)
            {
                editboolean = true;
                RepopulateMainForm?.Invoke(this, new ExistingContactInfo(listView_ContactList.Items.IndexOf(listView_ContactList.SelectedItems[0])));
                PassingBoolToEdit?.Invoke(this, new EditBoolean(editboolean));
            }
        }

        public int SelectedIndexFromListView
        {
            get { return listView_ContactList.Items.IndexOf(listView_ContactList.SelectedItems[0]); }
        }
    }
}
