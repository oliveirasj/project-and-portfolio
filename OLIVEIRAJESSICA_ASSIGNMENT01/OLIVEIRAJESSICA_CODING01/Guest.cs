﻿// NAME: JESSICA OLIVEIRA
// CLASS: PROJECT AND PORTFOLIO III
// TERM: JANUARY 2018
// EXERCISE: PARTY LIST (ASSIGNMENT 1)
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OLIVEIRAJESSICA_CODING01
{
    class Guest
    {
        public string name;
        public string phone;
        public string email;

        public override string ToString()
        {
            return name;
        }
    }
}
