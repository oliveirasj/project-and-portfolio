﻿// NAME: JESSICA OLIVEIRA
// CLASS: PROJECT AND PORTFOLIO III
// TERM: JANUARY 2018
// EXERCISE: PARTY LIST (ASSIGNMENT 1)
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace OLIVEIRAJESSICA_CODING01
{
    public partial class PartyList : Form
    {
        // Event handlers that will be used throughout this project
        public event EventHandler add_toSecondList;
        public event EventHandler add_toFirstList;
        public event EventHandler removeGuest;

        // Property to build a Guest object
        Guest invitedGuest
        {
            get
            {
                Guest guest = new Guest();
                guest.name = txtbox_name.Text;
                guest.phone = txtbox_phone.Text;
                guest.email = txtbox_email.Text;

                return guest;
            }
        }
        public PartyList()
        {
            InitializeComponent();

            // Assign the event handler to its correct method
            add_toFirstList += addToFirstList;
            removeGuest += removeGuests;
            add_toSecondList += addToSecondList;

        }

        private void Exit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Save_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveList = new SaveFileDialog();

            // Filter the type of file that will be saved
            saveList.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
            saveList.FilterIndex = 1;
            saveList.RestoreDirectory = true;

            if(saveList.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    using (StreamWriter sw = new StreamWriter(saveList.FileName))
                    {
                        sw.WriteLine("Invited Guests:");
                        foreach(Guest person in lbox_invited.Items)
                        {
                            sw.WriteLine(person.name);
                            sw.WriteLine(person.phone);
                            sw.WriteLine(person.email);
                        }

                        sw.WriteLine("Confirmed Guests:");
                        foreach(Guest person in lbox_confirmed.Items)
                        {
                            sw.WriteLine(person.name);
                            sw.WriteLine(person.phone);
                            sw.WriteLine(person.email);
                        }
                    }
                }
                catch(IOException a)
                {
                    // A proper message will be displayed according to the error
                    MessageBox.Show(a.Message);
                }
            }
            else
            {
                // Displays the message below in case the user attempts to save an empty file
                MessageBox.Show("Can't save empty fields!");
            }
        }

        private void button_addGuest_Click(object sender, EventArgs e)
        {
            // All created guests will be added to the "invited guests" list
            lbox_invited.Items.Add(invitedGuest);
            label_eventName.Text = txtbox_occasion.Text;
            label_eventDate.Text = txtbox_date.Text;

            // Clear the user input controls
            txtbox_occasion.Text = "";
            txtbox_date.Text = "";
            txtbox_name.Text = "";
            txtbox_phone.Text = "";
            txtbox_email.Text = "";
        }

        private void button_addToSecondList_Click(object sender, EventArgs e)
        {
            // To verify if the event handler "add_toSecondList" is not null
            if(add_toSecondList != null)
            {
                add_toSecondList(this, new EventArgs());
            }
        }

        private void addToSecondList(object sender, EventArgs e)
        {
            if(lbox_invited.SelectedItem != null)
            {
                // Moves the item from the "invited guests" list to the "confirmed guest" list
                lbox_confirmed.Items.Add(lbox_invited.SelectedItem);
                lbox_invited.Items.Remove(lbox_invited.SelectedItem);
            }
            else if (lbox_confirmed.SelectedItem != null)
            {
                MessageBox.Show("This guest is already in this list!");
            }

        }

        private void button_addToFirstList_Click(object sender, EventArgs e)
        {
            // To verify if the event handler "add_toFirstList" is not null
            if (add_toFirstList != null)
            {
                add_toFirstList(this, new EventArgs());
            }
        }

        private void addToFirstList(object sender, EventArgs e)
        {
            if(lbox_confirmed.SelectedItem != null)
            {
                // Moves the item from the "confirmed guest" list to the "invited guest" list
                lbox_invited.Items.Add(lbox_confirmed.SelectedItem);
                lbox_confirmed.Items.Remove(lbox_confirmed.SelectedItem);
            }
            else if (lbox_invited.SelectedItem != null)
            {
                MessageBox.Show("This guest is already in this list!");
            }
        }

        private void button_remove_Click(object sender, EventArgs e)
        {
            // To verify if the event handler "removeGuest" is not null
            if(removeGuest != null)
            {
                removeGuest(this, new EventArgs());
            }
        }

        private void removeGuests(object sender, EventArgs e)
        {
            // Verify which list the user selected the guest from to remove
            if(lbox_confirmed.SelectedItem != null)
            {
                lbox_confirmed.Items.Remove(lbox_confirmed.SelectedItem);
            }
            else if(lbox_invited.SelectedItem != null)
            {
                lbox_invited.Items.Remove(lbox_invited.SelectedItem);
            }
        }

        private void lbox_invited_DoubleClick(object sender, EventArgs e)
        {
            // Checks for a null value before repopulating the user input controls
            if(lbox_invited.SelectedItem != null)
            {
                txtbox_name.Text = ((Guest)lbox_invited.SelectedItem).name;
                txtbox_phone.Text = ((Guest)lbox_invited.SelectedItem).phone;
                txtbox_email.Text = ((Guest)lbox_invited.SelectedItem).email;
            }
        }

        private void lbox_confirmed_DoubleClick(object sender, EventArgs e)
        {
            // Checks for a null value before repopulating the user input controls
            if(lbox_confirmed.SelectedItem != null)
            {
                txtbox_name.Text = ((Guest)lbox_confirmed.SelectedItem).name;
                txtbox_phone.Text = ((Guest)lbox_confirmed.SelectedItem).phone;
                txtbox_email.Text = ((Guest)lbox_confirmed.SelectedItem).email;
            }
        }
    }
}
