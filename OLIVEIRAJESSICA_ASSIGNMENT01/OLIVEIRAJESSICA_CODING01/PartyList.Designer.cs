﻿namespace OLIVEIRAJESSICA_CODING01
{
    partial class PartyList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.Exit = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_remove = new System.Windows.Forms.Button();
            this.button_addToFirstList = new System.Windows.Forms.Button();
            this.button_addToSecondList = new System.Windows.Forms.Button();
            this.label_confirmed = new System.Windows.Forms.Label();
            this.label_invited = new System.Windows.Forms.Label();
            this.lbox_confirmed = new System.Windows.Forms.ListBox();
            this.lbox_invited = new System.Windows.Forms.ListBox();
            this.txtbox_email = new System.Windows.Forms.TextBox();
            this.txtbox_phone = new System.Windows.Forms.TextBox();
            this.txtbox_name = new System.Windows.Forms.TextBox();
            this.txtbox_date = new System.Windows.Forms.TextBox();
            this.txtbox_occasion = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label_phonenumber = new System.Windows.Forms.Label();
            this.label_name = new System.Windows.Forms.Label();
            this.label_date = new System.Windows.Forms.Label();
            this.label_occasion = new System.Windows.Forms.Label();
            this.Save = new System.Windows.Forms.ToolStripMenuItem();
            this.button_addGuest = new System.Windows.Forms.Button();
            this.label_eventName = new System.Windows.Forms.Label();
            this.label_eventDate = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(32, 32);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1210, 40);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Save,
            this.Exit});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(64, 36);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // Exit
            // 
            this.Exit.Name = "Exit";
            this.Exit.ShortcutKeyDisplayString = "CTRL+Q";
            this.Exit.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Q)));
            this.Exit.Size = new System.Drawing.Size(268, 38);
            this.Exit.Text = "Exit";
            this.Exit.Click += new System.EventHandler(this.Exit_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label_eventDate);
            this.groupBox1.Controls.Add(this.label_eventName);
            this.groupBox1.Controls.Add(this.button_addGuest);
            this.groupBox1.Controls.Add(this.button_remove);
            this.groupBox1.Controls.Add(this.button_addToFirstList);
            this.groupBox1.Controls.Add(this.button_addToSecondList);
            this.groupBox1.Controls.Add(this.label_confirmed);
            this.groupBox1.Controls.Add(this.label_invited);
            this.groupBox1.Controls.Add(this.lbox_confirmed);
            this.groupBox1.Controls.Add(this.lbox_invited);
            this.groupBox1.Controls.Add(this.txtbox_email);
            this.groupBox1.Controls.Add(this.txtbox_phone);
            this.groupBox1.Controls.Add(this.txtbox_name);
            this.groupBox1.Controls.Add(this.txtbox_date);
            this.groupBox1.Controls.Add(this.txtbox_occasion);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label_phonenumber);
            this.groupBox1.Controls.Add(this.label_name);
            this.groupBox1.Controls.Add(this.label_date);
            this.groupBox1.Controls.Add(this.label_occasion);
            this.groupBox1.Location = new System.Drawing.Point(12, 52);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1186, 1259);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Confirmation List";
            // 
            // button_remove
            // 
            this.button_remove.Location = new System.Drawing.Point(535, 1019);
            this.button_remove.Name = "button_remove";
            this.button_remove.Size = new System.Drawing.Size(89, 71);
            this.button_remove.TabIndex = 16;
            this.button_remove.Text = "X";
            this.button_remove.UseVisualStyleBackColor = true;
            this.button_remove.Click += new System.EventHandler(this.button_remove_Click);
            // 
            // button_addToFirstList
            // 
            this.button_addToFirstList.Location = new System.Drawing.Point(535, 844);
            this.button_addToFirstList.Name = "button_addToFirstList";
            this.button_addToFirstList.Size = new System.Drawing.Size(89, 71);
            this.button_addToFirstList.TabIndex = 15;
            this.button_addToFirstList.Text = "<<";
            this.button_addToFirstList.UseVisualStyleBackColor = true;
            this.button_addToFirstList.Click += new System.EventHandler(this.button_addToFirstList_Click);
            // 
            // button_addToSecondList
            // 
            this.button_addToSecondList.Location = new System.Drawing.Point(535, 678);
            this.button_addToSecondList.Name = "button_addToSecondList";
            this.button_addToSecondList.Size = new System.Drawing.Size(89, 71);
            this.button_addToSecondList.TabIndex = 14;
            this.button_addToSecondList.Text = ">>";
            this.button_addToSecondList.UseVisualStyleBackColor = true;
            this.button_addToSecondList.Click += new System.EventHandler(this.button_addToSecondList_Click);
            // 
            // label_confirmed
            // 
            this.label_confirmed.AutoSize = true;
            this.label_confirmed.Location = new System.Drawing.Point(658, 542);
            this.label_confirmed.Name = "label_confirmed";
            this.label_confirmed.Size = new System.Drawing.Size(184, 25);
            this.label_confirmed.TabIndex = 13;
            this.label_confirmed.Text = "Confirmed Guests";
            // 
            // label_invited
            // 
            this.label_invited.AutoSize = true;
            this.label_invited.Location = new System.Drawing.Point(27, 542);
            this.label_invited.Name = "label_invited";
            this.label_invited.Size = new System.Drawing.Size(149, 25);
            this.label_invited.TabIndex = 12;
            this.label_invited.Text = "Invited Guests";
            // 
            // lbox_confirmed
            // 
            this.lbox_confirmed.FormattingEnabled = true;
            this.lbox_confirmed.ItemHeight = 25;
            this.lbox_confirmed.Location = new System.Drawing.Point(663, 595);
            this.lbox_confirmed.Name = "lbox_confirmed";
            this.lbox_confirmed.Size = new System.Drawing.Size(472, 629);
            this.lbox_confirmed.TabIndex = 11;
            this.lbox_confirmed.DoubleClick += new System.EventHandler(this.lbox_confirmed_DoubleClick);
            // 
            // lbox_invited
            // 
            this.lbox_invited.FormattingEnabled = true;
            this.lbox_invited.ItemHeight = 25;
            this.lbox_invited.Location = new System.Drawing.Point(32, 595);
            this.lbox_invited.Name = "lbox_invited";
            this.lbox_invited.Size = new System.Drawing.Size(472, 629);
            this.lbox_invited.TabIndex = 10;
            this.lbox_invited.DoubleClick += new System.EventHandler(this.lbox_invited_DoubleClick);
            // 
            // txtbox_email
            // 
            this.txtbox_email.Location = new System.Drawing.Point(813, 148);
            this.txtbox_email.Name = "txtbox_email";
            this.txtbox_email.Size = new System.Drawing.Size(322, 31);
            this.txtbox_email.TabIndex = 9;
            // 
            // txtbox_phone
            // 
            this.txtbox_phone.Location = new System.Drawing.Point(211, 148);
            this.txtbox_phone.Name = "txtbox_phone";
            this.txtbox_phone.Size = new System.Drawing.Size(378, 31);
            this.txtbox_phone.TabIndex = 8;
            // 
            // txtbox_name
            // 
            this.txtbox_name.Location = new System.Drawing.Point(211, 90);
            this.txtbox_name.Name = "txtbox_name";
            this.txtbox_name.Size = new System.Drawing.Size(378, 31);
            this.txtbox_name.TabIndex = 7;
            // 
            // txtbox_date
            // 
            this.txtbox_date.Location = new System.Drawing.Point(813, 38);
            this.txtbox_date.Name = "txtbox_date";
            this.txtbox_date.Size = new System.Drawing.Size(322, 31);
            this.txtbox_date.TabIndex = 6;
            // 
            // txtbox_occasion
            // 
            this.txtbox_occasion.Location = new System.Drawing.Point(211, 41);
            this.txtbox_occasion.Name = "txtbox_occasion";
            this.txtbox_occasion.Size = new System.Drawing.Size(378, 31);
            this.txtbox_occasion.TabIndex = 5;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(629, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 25);
            this.label5.TabIndex = 4;
            this.label5.Text = "Email address:";
            // 
            // label_phonenumber
            // 
            this.label_phonenumber.AutoSize = true;
            this.label_phonenumber.Location = new System.Drawing.Point(27, 151);
            this.label_phonenumber.Name = "label_phonenumber";
            this.label_phonenumber.Size = new System.Drawing.Size(161, 25);
            this.label_phonenumber.TabIndex = 3;
            this.label_phonenumber.Text = "Phone Number:";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(27, 93);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(74, 25);
            this.label_name.TabIndex = 2;
            this.label_name.Text = "Name:";
            // 
            // label_date
            // 
            this.label_date.AutoSize = true;
            this.label_date.Location = new System.Drawing.Point(629, 44);
            this.label_date.Name = "label_date";
            this.label_date.Size = new System.Drawing.Size(63, 25);
            this.label_date.TabIndex = 1;
            this.label_date.Text = "Date:";
            // 
            // label_occasion
            // 
            this.label_occasion.AutoSize = true;
            this.label_occasion.Location = new System.Drawing.Point(27, 44);
            this.label_occasion.Name = "label_occasion";
            this.label_occasion.Size = new System.Drawing.Size(108, 25);
            this.label_occasion.TabIndex = 0;
            this.label_occasion.Text = "Occasion:";
            // 
            // Save
            // 
            this.Save.Name = "Save";
            this.Save.ShortcutKeyDisplayString = "CTRL+S";
            this.Save.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.Save.Size = new System.Drawing.Size(268, 38);
            this.Save.Text = "Save";
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // button_addGuest
            // 
            this.button_addGuest.Location = new System.Drawing.Point(447, 267);
            this.button_addGuest.Name = "button_addGuest";
            this.button_addGuest.Size = new System.Drawing.Size(321, 77);
            this.button_addGuest.TabIndex = 17;
            this.button_addGuest.Text = "Add Guest";
            this.button_addGuest.UseVisualStyleBackColor = true;
            this.button_addGuest.Click += new System.EventHandler(this.button_addGuest_Click);
            // 
            // label_eventName
            // 
            this.label_eventName.AutoSize = true;
            this.label_eventName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_eventName.Location = new System.Drawing.Point(477, 397);
            this.label_eventName.Name = "label_eventName";
            this.label_eventName.Size = new System.Drawing.Size(215, 37);
            this.label_eventName.TabIndex = 18;
            this.label_eventName.Text = "Event\'s Name";
            // 
            // label_eventDate
            // 
            this.label_eventDate.AutoSize = true;
            this.label_eventDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_eventDate.Location = new System.Drawing.Point(509, 461);
            this.label_eventDate.Name = "label_eventDate";
            this.label_eventDate.Size = new System.Drawing.Size(145, 29);
            this.label_eventDate.TabIndex = 19;
            this.label_eventDate.Text = "Event\'s Date";
            // 
            // PartyList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1210, 1329);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PartyList";
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem Exit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtbox_email;
        private System.Windows.Forms.TextBox txtbox_phone;
        private System.Windows.Forms.TextBox txtbox_name;
        private System.Windows.Forms.TextBox txtbox_date;
        private System.Windows.Forms.TextBox txtbox_occasion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_phonenumber;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_date;
        private System.Windows.Forms.Label label_occasion;
        private System.Windows.Forms.Button button_remove;
        private System.Windows.Forms.Button button_addToFirstList;
        private System.Windows.Forms.Button button_addToSecondList;
        private System.Windows.Forms.Label label_confirmed;
        private System.Windows.Forms.Label label_invited;
        private System.Windows.Forms.ListBox lbox_confirmed;
        private System.Windows.Forms.ListBox lbox_invited;
        private System.Windows.Forms.ToolStripMenuItem Save;
        private System.Windows.Forms.Button button_addGuest;
        private System.Windows.Forms.Label label_eventDate;
        private System.Windows.Forms.Label label_eventName;
    }
}

