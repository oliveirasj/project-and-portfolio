﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace TicTacToe
{
    public partial class frmTicTacToe : Form
    {
        // NAME: JESSICA OLIVEIRA
        // CLASS AND TERM: PORTFOLIO III, JANUARY 2018
        // PROJECT: Tic Tac Toe

        /* THINGS TO CONSIDER:
            - You must change the project name to conform to the required
              naming convention.
            - You must comment the code throughout.  Failure to do so could result
              in a lower grade.
            - All button names and other provided variables and controls must
              remain the same.  Changing these could result in a 0 on the project.
            - Selecting Blue or Red on the View menu should change the imageList
              attached to all buttons so that any current play will change the color
              of all button images.
            - Saved games should save to XML.  A game should load only from XML and
              should not crash the application if a user tries to load an incorrect 
              file.
        */

        // Variables to be used throughout the project
        // When the bool turn is set to true it's X's turn, when it's false it's O's turn
        bool turn = true;
        int turnCount = 0;

        // List of all the buttons
        List<Button> buttons = new List<Button>();

        public frmTicTacToe()
        {
            InitializeComponent();

            // Adding all the buttons to a list
            buttons.Add(r1c1button);
            buttons.Add(r1c2button);
            buttons.Add(r1c3button);
            buttons.Add(r2c1button);
            buttons.Add(r2c2button);
            buttons.Add(r2c3button);
            buttons.Add(r3c1button);
            buttons.Add(r3c2button);
            buttons.Add(r3c3button);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button_click(object sender, EventArgs e)
        {
            // We receive the button from where the user clicked
            Button clickedButton = (Button)sender;

            // This will not allow the user to overwrite the other player's move
            if (clickedButton.ImageIndex != 0 && clickedButton.ImageIndex != 1)
            {
                if (turn)
                {
                    clickedButton.ImageIndex = 1; // This is X
                    turn = !turn;
                    turnCount++;
                }
                else if(!turn)
                {
                    clickedButton.ImageIndex = 0; // This is O
                    turn = !turn;
                    turnCount++;
                }
            }
            else
            {
                if (turn)
                {
                    turn = true;
                }
                else if (!turn)
                {
                    turn = false;
                }
            }

            winnerValidation();
        }

        private void winnerValidation()
        {
            bool winner = false;

            // Checks for horizontal possible ways of winning the game
            if(r1c1button.ImageIndex == r1c2button.ImageIndex && r1c2button.ImageIndex == r1c3button.ImageIndex && r1c3button.ImageIndex != -1)
            {
                winner = true;
            }
            else if (r2c1button.ImageIndex == r2c2button.ImageIndex && r2c2button.ImageIndex == r2c3button.ImageIndex && r2c3button.ImageIndex != -1)
            {
                winner = true;
            }
            else  if (r3c1button.ImageIndex == r3c2button.ImageIndex && r3c2button.ImageIndex == r3c3button.ImageIndex && r3c3button.ImageIndex != -1)
            {
                winner = true;
            }

            // Checks for vertical possible ways of winning the game
            if (r1c1button.ImageIndex == r2c1button.ImageIndex && r2c1button.ImageIndex == r3c1button.ImageIndex && r3c1button.ImageIndex != -1)
            {
                winner = true;
            }
            else if (r1c2button.ImageIndex == r2c2button.ImageIndex && r2c2button.ImageIndex == r3c2button.ImageIndex && r3c2button.ImageIndex != -1)
            {
                winner = true;
            }
            else if (r1c3button.ImageIndex == r2c3button.ImageIndex && r2c3button.ImageIndex == r3c3button.ImageIndex && r3c3button.ImageIndex != -1)
            {
                winner = true;
            }

            // Checks for diagonal possible ways of winning the game
            if (r1c1button.ImageIndex == r2c2button.ImageIndex && r2c2button.ImageIndex == r3c3button.ImageIndex && r3c3button.ImageIndex != -1)
            {
                winner = true;
            }
            else if (r1c3button.ImageIndex == r2c2button.ImageIndex && r2c2button.ImageIndex == r3c1button.ImageIndex && r3c1button.ImageIndex != -1)
            {
                winner = true;
            }

            // Checks for the winner
            if (winner == true)
            {
                // Disable the rest of the buttons if the game is over
                disableButtonsWhenGameIsOver();

                string winnerPlayer = "";

                if (turn)
                {
                    winnerPlayer = "O";
                }
                else
                {
                    winnerPlayer = "X";
                }

                MessageBox.Show("Player " + winnerPlayer + " wins!");
            }
            else
            {
                if(turnCount == 9)
                {
                    MessageBox.Show("It's a draw!");
                }
            }
        }

        private void disableButtonsWhenGameIsOver()
        {
            foreach(Button btn in buttons)
            {
                btn.Enabled = false;
            }
        }
        private void blueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            blueToolStripMenuItem.Checked = true;
            redToolStripMenuItem.Checked = false;

            // It sets all the buttons to the same type of image list, in this case it's the blue color
            for(int i = 0; i < buttons.Count; i++)
            {
                buttons[i].ImageList = blueImages;
            }
        }

        private void redToolStripMenuItem_Click(object sender, EventArgs e)
        {
            redToolStripMenuItem.Checked = true;
            blueToolStripMenuItem.Checked = false;

            // It sets all the buttons to the same type of image list, in this case it's the red color
            for (int i = 0; i < buttons.Count; i++)
            {
                buttons[i].ImageList = redImages;
            }
        }

        private void xToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = true;

            xToolStripMenuItem.Checked = true;
            oToolStripMenuItem.Checked = false;
        }

        private void oToolStripMenuItem_Click(object sender, EventArgs e)
        {
            turn = false;

            oToolStripMenuItem.Checked = true;
            xToolStripMenuItem.Checked = false;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            turn = true;
            turnCount = 0;

            foreach (Button btn in buttons)
            {
                btn.Enabled = true;
                btn.ImageIndex = -1;
            }
        }

        private void saveGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog save = new SaveFileDialog();

            save.DefaultExt = "xml";

            if(DialogResult.OK == save.ShowDialog())
            {
                XmlWriterSettings settings = new XmlWriterSettings();
                settings.ConformanceLevel = ConformanceLevel.Document;

                settings.Indent = true;

                using(XmlWriter writer = XmlWriter.Create(save.FileName, settings))
                {
                    writer.WriteStartElement("MyTicTacToeGame");

                    // Saves the row 1 indexes
                    writer.WriteElementString("ButtonR1C1", r1c1button.ImageIndex.ToString());
                    writer.WriteElementString("ButtonR1C2", r1c2button.ImageIndex.ToString());
                    writer.WriteElementString("ButtonR1C3", r1c3button.ImageIndex.ToString());

                    // Saves the row 2 indexes
                    writer.WriteElementString("ButtonR2C1", r2c1button.ImageIndex.ToString());
                    writer.WriteElementString("ButtonR2C2", r2c2button.ImageIndex.ToString());
                    writer.WriteElementString("ButtonR2C3", r2c3button.ImageIndex.ToString());

                    // Saves the row 3 indexes
                    writer.WriteElementString("ButtonR3C1", r3c1button.ImageIndex.ToString());
                    writer.WriteElementString("ButtonR3C2", r3c2button.ImageIndex.ToString());
                    writer.WriteElementString("ButtonR3C3", r3c3button.ImageIndex.ToString());

                    writer.WriteEndElement();
                }
            }
        }

        private void loadGameToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();

            if(openFile.ShowDialog() == DialogResult.OK)
            {
                string extension = Path.GetExtension(openFile.FileName);

                if (extension.Contains(".xml"))
                {
                    try
                    {
                        XmlReaderSettings settings = new XmlReaderSettings();
                        settings.ConformanceLevel = ConformanceLevel.Document;

                        // Set to ignore whitespaces and comments
                        settings.IgnoreComments = true;
                        settings.IgnoreWhitespace = true;

                        using(XmlReader reader = XmlReader.Create(openFile.FileName, settings))
                        {
                            // Skips the metadata
                            reader.MoveToContent();

                            //Verify the identifier
                            if(reader.Name == "MyTicTacToeGame")
                            {
                                while (reader.Read())
                                {
                                    // Reads for the row 1 buttons
                                    if(reader.Name == "ButtonR1C1" && reader.IsStartElement())
                                    {
                                        string r1c1 = reader.ReadString();
                                        int br1c1 = Int32.Parse(r1c1);
                                        r1c1button.ImageIndex = br1c1;
                                    }
                                    if (reader.Name == "ButtonR1C2" && reader.IsStartElement())
                                    {
                                        string r1c2 = reader.ReadString();
                                        int br1c2 = Int32.Parse(r1c2);
                                        r1c2button.ImageIndex = br1c2;
                                    }
                                    if (reader.Name == "ButtonR1C3" && reader.IsStartElement())
                                    {
                                        string r1c3 = reader.ReadString();
                                        int br1c3 = Int32.Parse(r1c3);
                                        r1c3button.ImageIndex = br1c3;
                                    }

                                    // Reads for the row 2 buttons
                                    if (reader.Name == "ButtonR2C1" && reader.IsStartElement())
                                    {
                                        string r2c1 = reader.ReadString();
                                        int br2c1 = Int32.Parse(r2c1);
                                        r2c1button.ImageIndex = br2c1;
                                    }
                                    if (reader.Name == "ButtonR2C2" && reader.IsStartElement())
                                    {
                                        string r2c2 = reader.ReadString();
                                        int br2c2 = Int32.Parse(r2c2);
                                        r2c2button.ImageIndex = br2c2;
                                    }
                                    if (reader.Name == "ButtonR2C3" && reader.IsStartElement())
                                    {
                                        string r2c3 = reader.ReadString();
                                        int br2c3 = Int32.Parse(r2c3);
                                        r2c3button.ImageIndex = br2c3;
                                    }

                                    // Reads for the row 3 buttons
                                    if (reader.Name == "ButtonR3C1" && reader.IsStartElement())
                                    {
                                        string r3c1 = reader.ReadString();
                                        int br3c1 = Int32.Parse(r3c1);
                                        r3c1button.ImageIndex = br3c1;
                                    }
                                    if (reader.Name == "ButtonR3C2" && reader.IsStartElement())
                                    {
                                        string r3c2 = reader.ReadString();
                                        int br3c2 = Int32.Parse(r3c2);
                                        r3c2button.ImageIndex = br3c2;
                                    }
                                    if (reader.Name == "ButtonR3C3" && reader.IsStartElement())
                                    {
                                        string r3c3 = reader.ReadString();
                                        int br3c3 = Int32.Parse(r3c3);
                                        r3c3button.ImageIndex = br3c3;
                                    }
                                }
                            }
                        }
                    }
                    catch(IOException a)
                    {
                        MessageBox.Show(a.Message);
                    }
                }
            }
        }
    }
}
